====================
Picture Bingo
====================

Welcome to Picture Bingo, where the image is
the key to your winnings!

Picture Bingo acts a bingo caller with class.
It will display images at random which should
be paired with specialty bingo boards. You
decide what the images will be and let the
games begin. Images will not be repeated
until the game is reset. Feed it as many pics
as you want. It doesn't care.
