#!/usr/bin/env python2
# -*- encoding: utf-8 -*-
"""
This is the primary executable for Picture Bingo.
"""

import sys
import pygame
from pygame.locals import *
import pbgui
import imagepicker

size = (WIDTH, HEIGHT) = 600, 500
WHITE = (255, 255, 255)
BLACK = (0, 0, 0)

def getNextImage(screen, imgMan):
    image = imgMan.getImage()
    imageRect = None
    if image:
        xoffset = (screen.get_width() - image.get_width())/2
        yoffset = (screen.get_height() - image.get_height())/2
        imageRect = image.get_bounding_rect()
        imageRect = map(lambda x,y: x + y, imageRect, (xoffset, yoffset, xoffset, yoffset))
    return (image, imageRect)

def main():
    pygame.init()
    screen = pygame.display.set_mode(size)
    pygame.display.set_caption('Picture Bingo')
    screen.fill(WHITE)
    pygame.display.flip()

    imageManager = imagepicker.ImageListManager('./images/FamilyReunion/')
    halfWidth = WIDTH/2
    halfHeight = HEIGHT/2

    if pygame.font:
        font = pygame.font.Font('freesansbold.ttf', 36)
        font2 = pygame.font.Font('freesansbold.ttf', 56)
        title = font.render('Picture Bingo', True, (0, 0, 255))
        tTextPos = title.get_rect(centerx=screen.get_width()/2)
        resetText = font.render('Press space to restart the images.', True, BLACK)
        rTextPos = resetText.get_rect(centerx=screen.get_width()/2, centery=screen.get_height()/2)

    start = pbgui.Button((halfWidth-95-15, halfHeight-37, 95, 75), 'Start')
    quit = pbgui.Button((halfWidth-8, halfHeight-37, 95, 75), 'Quit')
    leftReset = halfWidth-40
    topReset = halfHeight-30
    reset = pbgui.Button((leftReset, topReset, 80, 60), 'Reset')
    #directions = pbgui.Button((50, 65, 115, 130), 'Instructions')
    clock = pygame.time.Clock()

    start.draw(screen)
    quit.draw(screen)
    #directions.draw(screen)

    screen.blit(title, tTextPos)

    while True:
        for event in pygame.event.get():
            if event.type == QUIT or (event.type == KEYDOWN and event.key == K_ESCAPE):
                pygame.quit()
                sys.exit()
            if event.type == pygame.KEYUP:
                if event.key == pygame.K_SPACE:
                    image, imageRect = getNextImage(screen, imageManager)
                    if image:
                        screen.fill(WHITE)
                        screen.blit(image, imageRect)
                        pygame.display.flip()
                    else:
                        screen.fill(WHITE)
                        reset.visible = True
                        reset.draw(screen)
                if event.key == pygame.K_r:
                    screen.fill(WHITE)
                    screen.blit(resetText, rTextPos)
                    imageManager.reset()

            if 'click' in start.handleEvent(event):
                image, imageRect = getNextImage(screen, imageManager)
                start.visible = False
                quit.visible = False
                screen.fill(WHITE)
                if image:
                    screen.blit(image, imageRect)
                    pygame.display.flip()
            if 'click' in reset.handleEvent(event):
                reset.visible = False
                screen.fill(WHITE)
                imageManager.reset()
                image, imageRect = getNextImage(screen, imageManager)
                if image:
                    screen.blit(image, imageRect)
                    pygame.display.flip()
                
            if 'click' in quit.handleEvent(event):
                pygame.quit()
                sys.exit()

        pygame.display.update()
        clock.tick(30)

if __name__ == '__main__':
    main()

