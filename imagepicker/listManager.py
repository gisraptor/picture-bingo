#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
This module manages the list of images for the Picture Bingo game.
"""

import os
import random
import pygame

class ImageListManager(object):

    images = []
    path = ''
    formats = None

    def __init__(self, path):
        """This method will initialize the list of images that will be displayed."""

        random.seed()
        self.path = path

        if pygame.image.get_extended():
            self.formats = ['.jpg', '.png', '.gif', '.bmp', '.pcx', '.tif']
        else:
            self.formats = ['.bmp']

        try:
            files = os.listdir(self.path)
            self.images = []
            for f in files:
                extension = os.path.splitext(f.lower())[1]
                if extension in self.formats:
                    self.images.append(os.path.join(self.path, f))
        except:
            raise

    def reset(self):
        try:
            files = os.listdir(self.path)
            self.images = []
            for f in files:
                extension = os.path.splitext(f.lower())[1]
                if extension in self.formats:
                    self.images.append(os.path.join(self.path, f))
        except:
            raise
        

    def getImage(self):
        """This method returns a PyGame surface with an image randomly drawn from
        the list of images not yet displayed."""
        imgsurface = None
        if len(self.images) > 0:
            index = random.randint(0,len(self.images)-1)
            imagepath = self.images.pop(index)
            try:
                imgsurface = pygame.image.load(imagepath)
                imgsurface = imgsurface.convert()
            except pygame.error, message:
                print 'Cannot load image:', name
                #raise SystemExit, message

        return imgsurface
